package savis.vn.account.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import savis.vn.account.commom.ResponseData;
import savis.vn.account.commom.Result;
import savis.vn.account.dto.request.PaymentIdentityRequestDTO;
import savis.vn.account.dto.request.PaymentRequestDTO;
import savis.vn.account.service.PaymentInitService;

@RestController
@RequestMapping("/savis-obh/pis")
public class PaymentInitEndPoint {

    @Autowired
    PaymentInitService paymentInitService;

    @PostMapping(value = "/payment/paymentInitiation/create")
    public ResponseEntity<ResponseData> initPayment(@RequestBody PaymentRequestDTO paymentRequestDTO) {
        ResponseData responseData = paymentInitService.initPayment(paymentRequestDTO);
        if (responseData.getCode() == Result.SUCCESS.getCode()) {
            return new ResponseEntity<>(responseData, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(responseData, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(value = "/payment/paymentInitiation/status")
    public ResponseEntity<ResponseData> getPaymentStatus(@RequestBody PaymentIdentityRequestDTO paymentIdentityRequestDTO) {
        ResponseData responseData = paymentInitService.getPaymentStatus(paymentIdentityRequestDTO);
        if (responseData.getCode() == Result.SUCCESS.getCode()) {
            return new ResponseEntity<>(responseData, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(responseData, HttpStatus.BAD_REQUEST);
        }
    }
}
