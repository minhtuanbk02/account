package savis.vn.account.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import savis.vn.account.commom.ResponseData;
import savis.vn.account.commom.Result;
import savis.vn.account.dto.request.TransactionDetailRequestDTO;
import savis.vn.account.dto.request.TransactionRequestDTO;
import savis.vn.account.service.TransactionService;

import java.time.LocalDate;

@RestController
@RequestMapping("/savis-obh/pis")
public class TransactionEndPoint {
    @Autowired
    TransactionService transactionService;

    @PostMapping("/transactions")
    public ResponseEntity<ResponseData> transactionHistory(@RequestBody TransactionRequestDTO transactionRequestDTO) {
        ResponseData responseData = transactionService.getTransactionHistory(transactionRequestDTO);
        if (responseData.getCode() == Result.SUCCESS.getCode()) {
            return new ResponseEntity<>(responseData, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(responseData, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/transactions/detail")
    public ResponseEntity<ResponseData> transactionHistory(@RequestBody TransactionDetailRequestDTO transactionDetailRequestDTO) {
        ResponseData responseData = transactionService.getTransactionDetail(transactionDetailRequestDTO);
        if (responseData.getCode() == Result.SUCCESS.getCode()) {
            return new ResponseEntity<>(responseData, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(responseData, HttpStatus.BAD_REQUEST);
        }
    }
}
