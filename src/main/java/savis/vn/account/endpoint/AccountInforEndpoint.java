package savis.vn.account.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import savis.vn.account.commom.ResponseData;
import savis.vn.account.commom.Result;
import savis.vn.account.dto.request.CustomerRequestDTO;
import savis.vn.account.service.AccountInforService;


@RestController
@RequestMapping("/savis-obh/ais")
public class AccountInforEndpoint {
    @Autowired
    AccountInforService accountInforService;

    @GetMapping(value = "/getAllBanks")
    public ResponseEntity<ResponseData> findAllBanks() {
        ResponseData response = accountInforService.getAllBanks();
        if (response.getCode() == Result.SUCCESS.getCode()) {
            return new ResponseEntity<>(response, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(value = "/customer")
    public ResponseEntity<ResponseData> getCustomerInfor(@RequestBody CustomerRequestDTO customerRequestDTO) {
        ResponseData response = accountInforService.getCustomerInfor(customerRequestDTO);
        if (response.getCode() == Result.SUCCESS.getCode()) {
            return new ResponseEntity<>(response, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping(value = "/account")
    public ResponseEntity<ResponseData> getAccountInfor(@RequestParam(value = "userName") String userName) {
        ResponseData response = accountInforService.getAccountInfor(userName);
        if (response.getCode() == Result.SUCCESS.getCode()) {
            return new ResponseEntity<>(response, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping(value = "{accountNumber}/balances")
    public ResponseEntity<ResponseData> getBalances(@PathVariable String accountNumber) {
        ResponseData response = accountInforService.getBalances(accountNumber);
        if (response.getCode() == Result.SUCCESS.getCode()) {
            return new ResponseEntity<>(response, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }
}
