package savis.vn.account.dto.response;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class TransactionDetailDTO {
    private String transactionId;
    private String accountNumber;
    private LocalDate transactionDate;
    private String transactionType;
    private String transactionStatus;
    private int transactionAmount;
    private String description;
    private String currency;
    private int balanceAfterOperation;

}
