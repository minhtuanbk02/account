package savis.vn.account.dto.response;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class CustomerInforDto {
    private String username;
    private String name;
    private String gender;
    private String email;
    private String phoneNumber;
    private String address;
    private String cccd;
    private List<AccountDto> account;
}
