package savis.vn.account.dto.response;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class TransactionHistoryDTO {
    private String accountNumber;
    private List<TransactionDetailDTO> listTransactions;
}
