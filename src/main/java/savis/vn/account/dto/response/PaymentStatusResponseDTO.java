package savis.vn.account.dto.response;

import lombok.Getter;
import lombok.Setter;
import savis.vn.account.entity.Transaction;

@Getter
@Setter
public class PaymentStatusResponseDTO {
    private Long id;
    private String accountNumber;
    private int amount;
    private String currency;
    private String paymentType;
    private String paymentStatus;
}
