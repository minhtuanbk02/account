package savis.vn.account.dto.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BalancesDto {
    private String accountNumber;
    private String accountName;
    private String currency;
    private int balance;
    private int availableBalance;
    private int lockedAmount;
}
