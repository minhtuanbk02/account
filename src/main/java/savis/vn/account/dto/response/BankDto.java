package savis.vn.account.dto.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BankDto {
    private String bankName;
    private String bankCode;
    private String url;
}
