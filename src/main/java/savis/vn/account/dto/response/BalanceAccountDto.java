package savis.vn.account.dto.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BalanceAccountDto {
    private String accountNumber;
    private int currentBalance;
    private int availableBalance;
    private int lockAmount;
    private String curencyCode;

}
