package savis.vn.account.dto.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AccountDto {
    private String accountNumber;
    private String accountName;
    private String productType;
    private String currency;
    private int balance;
    private int availableBalance;
    private int lockedAmount;
    private String postingRestriction;
    private String interestRate;
    private String lastTransactionDate;
    private String bankName;
    private String bankCode;
}
