package savis.vn.account.dto.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PaymentIdentityRequestDTO {
    private Long id;
}
