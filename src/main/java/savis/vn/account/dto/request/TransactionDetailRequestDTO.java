package savis.vn.account.dto.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TransactionDetailRequestDTO {
    private String accountNumber;
    private String transactionId;
}
