package savis.vn.account.dto.request;

import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;

@Getter
@Setter
public class TransactionRequestDTO {
    private String accountNumber;

    @DateTimeFormat(pattern = "yy-MM-dd")
    private LocalDate fromDate;

    @DateTimeFormat(pattern = "yy-MM-dd")
    private LocalDate toDate;
}
