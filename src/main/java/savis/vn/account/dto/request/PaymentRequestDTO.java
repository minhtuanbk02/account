package savis.vn.account.dto.request;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class PaymentRequestDTO {
    private String transactionId;
    private String accountNumber;
    private String paymentType;
    private int amount;
    private String currency;
    private LocalDate executionDate;
    private String description;
    private String status;
}
