package savis.vn.account.entity;


import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.time.LocalDate;

@Table(name = "PAYMENT_TBL")
@Entity
@Getter
@Setter
public class Payment {

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long paymentId;

    @Column(name = "ACCOUNT_NUMBER", nullable = false)
    private String accountNumber;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "TRANSACTION_ID")
    private Transaction transaction;

//    @Column(name = "TRANSACTION_ID")
//    private String transactionId;

    @Column(name = "PAYMENT_TYPE")
    private String paymentType;

    @Column(name = "AMOUNT")
    private int amount;

    @Column(name = "CURRENCY")
    private String currency;

    @Column(name = "EXCUTION_DATE")
    private LocalDate excutionDate;

    @Column(name = "DESCRIPTION")
    private String description;

    @Column(name = "PAYMENT_STATUS")
    private String status;

}
