package savis.vn.account.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.sql.Date;
import java.time.LocalDate;


@Table(name = "TRANSACTION_TBL")
@Entity
@Getter
@Setter
public class Transaction {
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "TRANSACTION_ID")
    private String bankTransactionID;

    @Column(name = "TRANSACTION_DATE")
    private LocalDate transactionDate;

    @Column(name = "ACCOUNT_NUMBER")
    private String sourceAccountNumber;

    @Column(name = "AMOUNT")
    private int transactionAmount;

    @Column(name = "DESCRIPTION")
    private String description;

    @Column(name = "CURENCY")
    private String currency;

    @Column(name = "TRANSACTION_TYPE")
    private String type;

    @Column(name = "TRANSACTION_STATUS")
    private String status;

    @Column(name = "BALANCE_AFTER_OPERATION")
    private int balanceAfterOperation;

    @Column(name = "BENEFIT_ACCOUNT_NUMBER")
    private String benefitAccountNumber;

    @Column(name = "TRANSACTON_VAL_TIME")
    private Date transactionValTime;

    @Column(name = "TRANSACTION_ACTIVE_DATE")
    private Date transactionActiveDate;

    @Column(name = "APPROVED_BY")
    private String approvedBy;


}
