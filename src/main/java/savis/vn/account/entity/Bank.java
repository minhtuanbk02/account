package savis.vn.account.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(
        name = "BANKS"
)
@Setter
@Getter
public class Bank {
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "BANK_NAME")
    private String bankName;

    @Column(name = "BANK_CODE")
    private String bankCode;

    @Column(name = "URL")
    private String url;

    @Column(name="CREATED_BY")
    private String createdBy;

    @Column(name="CREATED_TIME")
    private Date createdTime;

    @Column(name="UPDATED_BY")
    private String updatedBy;

    @Column(name="UPDATED_TIME")
    private Date updatedTime;
}
