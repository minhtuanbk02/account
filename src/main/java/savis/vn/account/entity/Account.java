package savis.vn.account.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(
        name = "ACCOUNT"
)
@Setter
@Getter
public class Account {
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "ACCOUNT_NUMBER")
    private String accountNumber;

    @Column(name = "ACCOUNT_NAME")
    private String accountName;

    @Column(name = "USER_NAME")
    private String userName;

    @Column(name = "PRODUCT_TYPE")
    private String productType;

    @Column(name = "CURRENCY")
    private String currency;

    @Column(name = "BALANCE")
    private int balance;

    @Column(name = "AVAILABLE_BALANCE")
    private int availableBalance;

    @Column(name = "LOCKED_AMOUNT")
    private int lockedAmount;

    @Column(name = "POSTING_RESTRICTION")
    private String postingRestriction;

    @Column(name = "INTEREST_RATE")
    private String interestRate;

    @Column(name = "LAST_TRANSACTION_DATE")
    private String lastTransactionDate;

    @Column(name = "BANK_NAME")
    private String bankName;

    @Column(name = "BANK_CODE")
    private String bankCode;

    @Column(name = "USER_ID")
    private long userId;

    @Column(name="CREATED_BY")
    private String createdBy;

    @Column(name="CREATED_TIME")
    private Date createdTime;

    @Column(name="UPDATED_BY")
    private String updatedBy;

    @Column(name="UPDATED_TIME")
    private Date updatedTime;
}
