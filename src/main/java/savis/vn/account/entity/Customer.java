package savis.vn.account.entity;

import lombok.Getter;
import lombok.Setter;
import javax.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.Set;

@Entity
@Table(
        name = "CUSTOMER"
)
@Setter
@Getter
public class Customer {
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "USER_NAME")
    private String userName;

    @Column(name = "NAME")
    private String name;

    @Column(name = "GENDER")
    private int gender;

    @Column(name = "EMAIL")
    private String email;
    @Column(name = "PHONENUMBER")
    private String phoneNumber;

    @Column(name = "ADDRESS")
    private String address;

    @Column(name = "CCCD")
    private String cccd;

    @Column(name="CREATED_BY")
    private String createdBy;

    @Column(name="CREATED_TIME")
    private Date createdTime;

    @Column(name="UPDATED_BY")
    private String updatedBy;

    @Column(name="UPDATED_TIME")
    private Date updatedTime;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "fk_ID",referencedColumnName = "ID")
    private List<Account> account;
}
