package savis.vn.account.service.Impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import savis.vn.account.commom.ResponseData;
import savis.vn.account.commom.Result;
import savis.vn.account.dto.response.TransactionDetailDTO;
import savis.vn.account.dto.response.TransactionHistoryDTO;
import savis.vn.account.dto.request.TransactionDetailRequestDTO;
import savis.vn.account.dto.request.TransactionRequestDTO;
import savis.vn.account.entity.Transaction;
import savis.vn.account.repository.TransactionRepository;
import savis.vn.account.service.TransactionService;
import java.util.ArrayList;
import java.util.List;

@Service
public class TransactionServiceImpl implements TransactionService {

    @Autowired
    TransactionRepository transactionRepository;

    @Override
    public ResponseData<String> getTransactionHistory(TransactionRequestDTO transactionRequestDTO) {
        List<Transaction> listTransactions = transactionRepository.findAllBySourceAccountNumberAndTransactionDateBetween(
                transactionRequestDTO.getAccountNumber(),
                transactionRequestDTO.getFromDate(),
                transactionRequestDTO.getToDate());
        List<TransactionDetailDTO> listTransactionDetailDTO = new ArrayList<>();
        if (listTransactions != null) {
            for (Transaction transaction : listTransactions) {
                TransactionDetailDTO transactionDetailDTO = transactionDetail(transaction);
                listTransactionDetailDTO.add(transactionDetailDTO);
            }
            TransactionHistoryDTO transactionHistoryDTO = new TransactionHistoryDTO();
            transactionHistoryDTO.setAccountNumber(transactionRequestDTO.getAccountNumber());
            transactionHistoryDTO.setListTransactions(listTransactionDetailDTO);
            ResponseData response = new ResponseData<>(Result.SUCCESS);
            response.setData(transactionHistoryDTO);
            return response;
        } else {
            ResponseData response = new ResponseData<>(Result.BAD_REQUEST);
            response.setData(null);
            return response;
        }
    }

    @Override
    public ResponseData<String> getTransactionDetail(TransactionDetailRequestDTO transactionDetailRequestDTO) {
        Transaction transaction = transactionRepository.findAllBySourceAccountNumberAndBankTransactionID(transactionDetailRequestDTO.getAccountNumber(), transactionDetailRequestDTO.getTransactionId());
        if (transaction != null) {
            TransactionDetailDTO transactionDetailDTO = transactionDetail(transaction);
            ResponseData responseData = new ResponseData<>(Result.SUCCESS);
            responseData.setData(transactionDetailDTO);
            return responseData;
        } else {
            ResponseData responseData = new ResponseData<>(Result.BAD_REQUEST);
            responseData.setData(null);
            return responseData;
        }
    }

    public TransactionDetailDTO transactionDetail(Transaction transaction) {
        TransactionDetailDTO transactionDetailDTO = new TransactionDetailDTO();
        transactionDetailDTO.setTransactionId(transaction.getBankTransactionID());
        transactionDetailDTO.setAccountNumber(transaction.getSourceAccountNumber());
        transactionDetailDTO.setTransactionDate(transaction.getTransactionDate());
        transactionDetailDTO.setTransactionType(transaction.getType());
        transactionDetailDTO.setTransactionStatus(transaction.getStatus());
        transactionDetailDTO.setTransactionAmount(transaction.getTransactionAmount());
        transactionDetailDTO.setDescription(transaction.getDescription());
        transactionDetailDTO.setCurrency(transaction.getCurrency());
        transactionDetailDTO.setBalanceAfterOperation(transaction.getBalanceAfterOperation());
        return transactionDetailDTO;
    }
}
