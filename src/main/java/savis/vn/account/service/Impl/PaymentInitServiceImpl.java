package savis.vn.account.service.Impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import savis.vn.account.commom.ResponseData;
import savis.vn.account.commom.Result;
import savis.vn.account.dto.response.PaymentStatusResponseDTO;
import savis.vn.account.dto.request.PaymentIdentityRequestDTO;
import savis.vn.account.dto.request.PaymentRequestDTO;
import savis.vn.account.entity.Account;
import savis.vn.account.entity.Payment;
import savis.vn.account.entity.Transaction;
import savis.vn.account.repository.AccountRepository;
import savis.vn.account.repository.PaymentRepository;
import savis.vn.account.repository.TransactionRepository;
import savis.vn.account.service.PaymentInitService;


@Service
public class PaymentInitServiceImpl implements PaymentInitService {

    @Autowired
    PaymentRepository paymentRepository;
    @Autowired
    TransactionRepository transactionRepository;
    @Autowired
    AccountRepository accountRepository;


    @Transactional
    @Override
    public ResponseData<String> initPayment(PaymentRequestDTO paymentRequestDTO) {
        try {
            Account account = accountRepository.getAccountByAccountNumber(paymentRequestDTO.getAccountNumber());
            if (account.getAvailableBalance() >= paymentRequestDTO.getAmount()) {

                //update balance after payment
                int balanceAfterOperation = account.getAvailableBalance() - paymentRequestDTO.getAmount();


                //save transaction after payment
                Transaction transaction = new Transaction();
                transaction.setBankTransactionID(paymentRequestDTO.getTransactionId());
                transaction.setSourceAccountNumber(paymentRequestDTO.getAccountNumber());
                transaction.setTransactionDate(paymentRequestDTO.getExecutionDate());
                transaction.setType("Payment");
                transaction.setStatus("ACCEPT");
                transaction.setTransactionAmount(paymentRequestDTO.getAmount());
                transaction.setCurrency(paymentRequestDTO.getCurrency());
                transaction.setDescription(paymentRequestDTO.getDescription());
                transaction.setBalanceAfterOperation(balanceAfterOperation);
                transactionRepository.save(transaction);
                //init payment
                Payment payment = new Payment();
                payment.setPaymentType(paymentRequestDTO.getPaymentType());
                payment.setAccountNumber(paymentRequestDTO.getAccountNumber());
                payment.setAmount(paymentRequestDTO.getAmount());
                payment.setCurrency(paymentRequestDTO.getCurrency());
                payment.setExcutionDate(paymentRequestDTO.getExecutionDate());
                payment.setDescription(paymentRequestDTO.getDescription());
                payment.setStatus(paymentRequestDTO.getStatus());
                payment.setTransaction(transaction);
                paymentRepository.save(payment);
                updateBalancesAfterPayment(account.getId(), balanceAfterOperation);

                ResponseData responseData = new ResponseData<>(Result.SUCCESS);
                responseData.setData(paymentRequestDTO);
                return responseData;
            } else {
                ResponseData responseData = new ResponseData<>(Result.CHECK_AMOUNT_FAILED);
                return responseData;
            }
        } catch (Exception e) {
            ResponseData responseData = new ResponseData<>(Result.BAD_REQUEST);
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return responseData;
        }
    }

    @Override
    public ResponseData<String> getPaymentStatus(PaymentIdentityRequestDTO paymentIdentityRequestDTO) {
        Payment payment = paymentRepository.getById(paymentIdentityRequestDTO.getId());
        if(payment != null) {
            PaymentStatusResponseDTO paymentStatusResponseDTO = new PaymentStatusResponseDTO();
            paymentStatusResponseDTO.setId(payment.getPaymentId());
            paymentStatusResponseDTO.setAccountNumber(payment.getAccountNumber());
            paymentStatusResponseDTO.setAmount(payment.getAmount());
            paymentStatusResponseDTO.setCurrency(payment.getCurrency());
            paymentStatusResponseDTO.setPaymentType(payment.getPaymentType());
            paymentStatusResponseDTO.setPaymentStatus(payment.getStatus());
            ResponseData responseData = new ResponseData<>(Result.SUCCESS);
            responseData.setData(paymentStatusResponseDTO);
            return  responseData;
        } else {
            ResponseData responseData = new ResponseData<>(Result.BAD_REQUEST);
            responseData.setData(null);
            return  responseData;
        }
    }

    @Override
    public void updateBalancesAfterPayment(Long id, int balance) {
        Account account = accountRepository.getById(id);
        account.setAvailableBalance(balance);
        int totalBalance = balance + account.getLockedAmount();
        account.setBalance(totalBalance);
        accountRepository.save(account);
    }
}
