package savis.vn.account.service.Impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import savis.vn.account.commom.ResponseData;
import savis.vn.account.commom.Result;
import savis.vn.account.dto.response.AccountDto;
import savis.vn.account.dto.response.BalancesDto;
import savis.vn.account.dto.response.BankDto;
import savis.vn.account.dto.response.CustomerInforDto;
import savis.vn.account.dto.request.CustomerRequestDTO;
import savis.vn.account.entity.Account;
import savis.vn.account.entity.Bank;
import savis.vn.account.entity.Customer;
import savis.vn.account.repository.AccountRepository;
import savis.vn.account.repository.BankRepository;
import savis.vn.account.repository.CustomerRepository;
import savis.vn.account.service.AccountInforService;
import org.apache.log4j.Logger;

import java.util.*;

@Service
public class AccountInforServiceImpl implements AccountInforService {

    @Autowired
    CustomerRepository customerRepository;
    @Autowired
    AccountRepository accountRepository;
    @Autowired
    BankRepository bankRepository;

    private static final Logger logger = Logger.getLogger(AccountInforServiceImpl.class);
    @Override
    public ResponseData<String> getCustomerInfor(CustomerRequestDTO customerRequestDTO) {
        List<Account> accountList = accountRepository.findAllByUserName(customerRequestDTO.getUserName());
        List<AccountDto> listAccount = new ArrayList<>();
        for(Account account : accountList){
            AccountDto accountDto = getAccountDto(account);
            listAccount.add(accountDto);
        }
        Customer customer = customerRepository.findFirstByUserName(customerRequestDTO.getUserName());
        if (customer != null){
            CustomerInforDto customerInforDto = new CustomerInforDto();
            customerInforDto.setUsername(customer.getUserName());
            customerInforDto.setName(customer.getName());
            customerInforDto.setEmail(customer.getEmail());
            customerInforDto.setPhoneNumber(customer.getPhoneNumber());
            customerInforDto.setAddress(customer.getAddress());
            customerInforDto.setCccd(customer.getCccd());
            customerInforDto.setAccount(listAccount);
            if (customer.getGender() == 0){
                customerInforDto.setGender("male");
            }else {
                customerInforDto.setGender("female");
            }
            ResponseData response = new ResponseData<>(Result.SUCCESS);
            response.setData(customerInforDto);
            return response;
        }else {
            ResponseData response = new ResponseData<>(Result.USER_FOUND);
            response.setData(null);
            return response;
        }
    }

    @Override
    public ResponseData<String> getAccountInfor(String userName) {
        List <Account> accounts = accountRepository.findAllByUserName(userName);
        List<AccountDto> listAccount = new ArrayList<>();
        if (accounts.size() > 0){
            for (Account account: accounts) {
                AccountDto accountDto = new AccountDto();
                accountDto.setAccountName(account.getAccountName());
                accountDto.setAccountNumber(account.getAccountNumber());
                accountDto.setBalance(account.getBalance());
                accountDto.setAvailableBalance(account.getAvailableBalance());
                accountDto.setCurrency(account.getCurrency());
                accountDto.setLockedAmount(account.getLockedAmount());
                accountDto.setBankName(account.getBankName());
                accountDto.setBankCode(account.getBankCode());
                accountDto.setInterestRate(account.getInterestRate());
                accountDto.setLastTransactionDate(account.getLastTransactionDate());
                accountDto.setProductType(account.getProductType());
                accountDto.setPostingRestriction(account.getPostingRestriction());
                listAccount.add(accountDto);
            }
            ResponseData response = new ResponseData<>(Result.SUCCESS);
            response.setData(listAccount);
            return response;
        }else {
            ResponseData response = new ResponseData<>(Result.USER_FOUND);
            response.setData(null);
            return response;
        }
    }

    @Override
    public ResponseData<String> getAllBanks() {
        List<Bank> banks = bankRepository.findAll();
        List<BankDto> banksDto = new ArrayList<>();
        if (banks != null){
            for ( Bank bank :banks) {
                BankDto bankDto = new BankDto();
                bankDto.setBankCode(bank.getBankCode());
                bankDto.setBankName(bank.getBankName());
                bankDto.setUrl(bank.getUrl());
                banksDto.add(bankDto);
            }
            ResponseData response = new ResponseData<>(Result.SUCCESS);
            response.setData(banksDto);
            return response;
        }else {
            ResponseData response = new ResponseData<>(Result.USER_FOUND);
            response.setData(null);
            return response;
        }
    }

    @Override
    public ResponseData<String> getBalances(String accountNumber) {
       Account accounts = accountRepository.getAccountByAccountNumber(accountNumber);
        if (accounts != null){
            BalancesDto balancesDto = new BalancesDto();
            balancesDto.setAccountNumber(accounts.getAccountNumber());
            balancesDto.setAccountName(accounts.getAccountName());
            balancesDto.setBalance(accounts.getBalance());
            balancesDto.setAvailableBalance(accounts.getAvailableBalance());
            balancesDto.setCurrency(accounts.getCurrency());
            balancesDto.setLockedAmount(accounts.getLockedAmount());
            ResponseData response = new ResponseData<>(Result.SUCCESS);
            response.setData(balancesDto);
            return response;
        }else {
            ResponseData response = new ResponseData<>(Result.USER_FOUND);
            response.setData(null);
            return response;
        }
    }

    @Override
    public void updateBalances(Long id, int balance) {

    }

    public AccountDto getAccountDto(Account account){
        AccountDto accountDto = new AccountDto();
        accountDto.setAccountName(account.getAccountName());
        accountDto.setAccountNumber(account.getAccountNumber());
        accountDto.setBalance(account.getBalance());
        accountDto.setAvailableBalance(account.getAvailableBalance());
        accountDto.setCurrency(account.getCurrency());
        accountDto.setLockedAmount(account.getLockedAmount());
        accountDto.setBankName(account.getBankName());
        accountDto.setBankCode(account.getBankCode());
        accountDto.setInterestRate(account.getInterestRate());
        accountDto.setLastTransactionDate(account.getLastTransactionDate());
        accountDto.setProductType(account.getProductType());
        accountDto.setPostingRestriction(account.getPostingRestriction());
        return accountDto;
    }
}
