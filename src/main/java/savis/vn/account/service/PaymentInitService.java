package savis.vn.account.service;

import savis.vn.account.commom.ResponseData;
import savis.vn.account.dto.request.PaymentIdentityRequestDTO;
import savis.vn.account.dto.request.PaymentRequestDTO;
import savis.vn.account.entity.Account;

public interface PaymentInitService {
    ResponseData<String> initPayment(PaymentRequestDTO paymentRequestDTO);

    ResponseData<String> getPaymentStatus(PaymentIdentityRequestDTO paymentIdentityRequestDTO);
    void updateBalancesAfterPayment(Long id, int balance);
}
