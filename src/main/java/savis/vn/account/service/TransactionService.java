package savis.vn.account.service;

import org.springframework.stereotype.Service;
import savis.vn.account.commom.ResponseData;
import savis.vn.account.dto.request.TransactionDetailRequestDTO;
import savis.vn.account.dto.request.TransactionRequestDTO;

import java.time.LocalDate;

public interface TransactionService {
    ResponseData<String> getTransactionHistory(TransactionRequestDTO transactionRequestDTO);

    ResponseData<String> getTransactionDetail(TransactionDetailRequestDTO transactionDetailRequestDTO);
}
