package savis.vn.account.service;

import savis.vn.account.commom.ResponseData;
import savis.vn.account.dto.request.CustomerRequestDTO;

public interface AccountInforService {
    ResponseData<String> getCustomerInfor (CustomerRequestDTO customerRequestDTO);

    ResponseData<String> getAccountInfor (String userName);

    ResponseData<String> getAllBanks ();

    ResponseData<String> getBalances (String accountNumber);

    void updateBalances(Long id, int balance);
}
