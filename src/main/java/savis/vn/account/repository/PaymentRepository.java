package savis.vn.account.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import savis.vn.account.entity.Payment;

public interface PaymentRepository extends JpaRepository<Payment,Long> {
}
