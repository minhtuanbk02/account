package savis.vn.account.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import savis.vn.account.entity.Transaction;

import java.time.LocalDate;
import java.util.List;

public interface TransactionRepository extends JpaRepository<Transaction, Long> {
    List<Transaction> findAllBySourceAccountNumberAndTransactionDateBetween(String accountNumber, LocalDate fromDate, LocalDate toDate);

    Transaction findAllBySourceAccountNumberAndBankTransactionID(String accountNumber, String transactionID);
}
