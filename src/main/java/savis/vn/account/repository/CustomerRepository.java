package savis.vn.account.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import savis.vn.account.entity.Customer;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long> {
    Customer findAllById (Long id);
    Customer findFirstByUserName (String userName);

}
