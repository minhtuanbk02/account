package savis.vn.account.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import savis.vn.account.entity.Account;

import java.util.List;

public interface AccountRepository extends JpaRepository<Account, Long> {
     Account getAccountByAccountNumber(String accountId);

     List<Account> findAllByUserName(String username);


}
