package savis.vn.account.commom;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ResponseData<T> {

    private int code;
    private String message;
    private T Data;

    public ResponseData(HttpStatus result) {
        this.code = result.value();
        this.message = result.getReasonPhrase();
    }

    public ResponseData(T data, HttpStatus result) {
        this.code = result.value();
        this.message = result.getReasonPhrase();
        this.Data = data;
    }

    public ResponseData(Result result) {
        this.code = result.getCode();
        this.message = result.getMessage();
    }

    public ResponseData(T data, Result result) {
        this.Data = data;
        this.code = result.getCode();
        this.message = result.getMessage();
    }

    @JsonIgnore
    public boolean isSucess() {
        return (this.code == 1);
    }

}

