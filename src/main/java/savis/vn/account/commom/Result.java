package savis.vn.account.commom;

public enum Result {

	SUCCESS(0000, "Success"),
	NO_CONTENT(422, "Id not null"),
	USER_FOUND(302, "Người dùng không tồn tại trong hệ thống!"),
	BAD_REQUEST(400, "Bad request"),
	UNAUTHORIZED(401, "Unauthorized"),
	TOKEN_EXPIRE_TIME(401, "Token expire time"),
	FORBIDDEN(403, "Forbidden"),
	NOT_FOUND(404, "Api not found"),
	METHOD_NOT_ALLOW(405, "Method not allow"),
	SEND_MAIL_FAILED (412, "Gửi Email không thành công, vui lòng kiểm tra lại!"),
	CHECK_OTP_FAILED (412, "Xác nhận OTP không hợp lệ!"),
	CHECK_AMOUNT_FAILED (413, "Số dư không đủ để thanh toán!"),
    SERVER_ERROR(500, "500 Internal server error"),
	USER_EXITS (409, "Tài khoản đã tồn tại!"),
	NAME_EXITS(409, "Tên tham số đã tồn tại."),
	PASSWORD_PATTERN(500, "Mật khẩu không hợp lệ"),
	PASSWORD_NEW_PATTERN(500, "Mật khẩu mới không hợp lệ"),
    DELETE_GROUPAPI_NOT(500, "Không thể xóa nhóm API do có API đã đăng ký nhóm"),
	USER_APPLICATION_NULL(409, "Ứng dụng không được để trống!"),
	;

	private int code;
	private String message;

	Result(int code, String message){
		this.code = code;
		this.message = message;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public boolean isSuccess() {
		return (this.code==1);
	}

}
